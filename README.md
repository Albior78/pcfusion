Pour problème php de composer
#09/03/2021
> Changer dans composer json "php": ">=7.1.3", en "php": "^7.3|^8.0",
> Lancer la commande composer update

09/03/2021
installation bundle easyadmin (pour fabriquer et gerer en back-office)
voir les bundles sur le site https://packagist.org/
chercher easycorp/easyadmin-bundle
avec laragon:
dans le fichier composer.lock à la ligne:
            "name": "psr/link",
            "version": "1.1.0", mettre "version": "1.0.0"
et lancer un: composer update
ensuite
dans un terminal lancer la commande:
composer require easycorp/easyadmin-bundle v2.3.12

*mis en place bundle: ckeditor v4.6.0
cde:
composer require ckeditor/ckeditor v4.6.0

*mis en place bundle: vich/uploader v4.16.1
cde:
composer require vich/uploader-bundle v4.16.1

10/03/2021
suite problèmes bundles:

*suppression de ckeditor:
cde:
composer remove friendsofsymfony/ckeditor-bundle

*suppression de vich/uploader
cde:
composer remove vich/uploader-bundle

*reinstallation de vich/uploader en mettant 'yes' (tjrs mettre 'yes' à la fin d'installation d'un bundle):
cde:
composer require  vich/uploader-bundle
yes

*installation de friendsofsymfony/ckeditor-bundle avec yes:
cde:
composer require friendsofsymfony/ckeditor-bundle
yes
    installarion de ckeditor:
    cde:
    php bin/console ckeditor:install

*installation des assets:
    cde:
    php bin/console assets:install

*installation bundle orm-fixtures (permet de placer des données fictives dans la BDD pour essais):
cde:
composer require --dev orm-fixtures
-pour ecrire des fixtures (c'est dans AppFixtures.php):
scr/DataFixtures/AppFixtures.php
-création du fichier LogoFixture.php dans le dossier src/DataFixture/
-mise en place du code suivant -> voir fichier LogoFixture.php
-mise en place des données fictives dans la table logo:
cde
 php bin/console doctrine:fixtures:load
 Careful, database "pcfusion" will be purged. Do you want to continue? (yes/no) [no]:
 'yes' (suppression de ce qu'il y a dans la BDD) et réecriture des données fictives
 on peut utiliser également la cde:
 php bin/console doctrine:fixtures:load --append
 qui va rajouter données à la suite des autres présentent dans la base.




