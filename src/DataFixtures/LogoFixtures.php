<?php

namespace App\DataFixtures;

use App\Entity\Logo;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class LogoFixtures extends Fixture
{
    //Méthodes
    public function load(ObjectManager $manager)
    {
        $logo = new Logo();
        // on créé jusqu'a 3 logos
        for ($i = 0; $i < 4; $i++){
            $logo = new Logo();
            $logo->setNom('Logo '.$i);
            $logo->setImage('logo_'.$i.'.jpg');
            $logo->setUpdateAt(new DateTime('now'));
            $logo->setActive(0);
            $logo->setRank($i);
            // on met en mémoire
            $manager->persist($logo);
        }
        // $product = new Product();
        // $manager->persist($product);
        $manager->flush();
    }
}